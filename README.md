# Face Detection Controlled Media Player

This is a project which uses haar casade to detect face position and control the media player accordingly. If the user is seeing the screen the media plays but as soon as the user changes his face position ( looks away from the screen ) the media player pauses and waits until the user see's back to the screen.

## Packages and software required to run the application
<ul>
<li>Python 2</li>
<li>Open CV</li>
<li>vlc-ctrl</li>
<li>VLC Media player</li>
</ul>

## Future vision of the project

This project can be further enhanced to detect the eye movement and control the media player according to the gestures.

## Links to the packages and method to install vlc-ctrl
<ul>
<li>This has a detailed procedure for instaling Open CV in Linux * http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html</li>

<li>Installing vlc-ctrl packagae for linux using pip install * pip2 install vlc-ctrl ( If you have both python2 and python3 installed ) * pip install vlc-ctrl ( If you have only python2 installed )</li>

<li>I have used Python 2 because I had installed opencv in it.</li>

<li>Any VLC Media player is fine for running the application just make sure that it is not too outdated.</li>

<li>You can get the haar cascade classifier from the link https://github.com/opencv/opencv/tree/master/data/haarcascades</li>
</ul>

## Restrictions of the developed application
<ul>
<li>This system is developed only for linux based systems.</li>
<li>You should put your video which you want to play in the folder assigned by me.</li>
<li>Must download a shell file which will be running in background for checking if you have clicked VLC Media player.</li>
<li>Works only for VLC Media player.</li>
<li>The lighting of the room and camera resolution effects the program.</li>
</ul>

![alt text](images/abc.png?raw=true "Optional Title")

